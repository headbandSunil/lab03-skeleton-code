package course.labs.activitylab
import java.util.Calendar;
import java.util.regex.Pattern

class KotlinFunctions {

    fun dayOfWeek(): String {
        val currentDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        return when (currentDay){
            1 -> "Sunday"
            2 -> "Monday"
            3 -> "Tuesday"
            4 -> "Wednesday"
            5 -> "Thursday"
            6 -> "Friday"
            7 -> "Saturday"
            else -> "No day in our world"
        }
    }

    fun dayOfWeekSingleExp() = when (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)){
        1 -> "Sunday"
        2 -> "Monday"
        3 -> "Tuesday"
        4 -> "Wednesday"
        5 -> "Thursday"
        6 -> "Friday"
        7 -> "Saturday"
        else -> "No day in our world"
    }


    fun isValidIdentifier(s:String): Boolean{
        val pattern = Pattern.compile("^([a-zA-Z]|_)\\w*")
        //val matches = pattern.matcher(s).matches()
        return when {
            s == "" -> false
            /*s[0].toUpperCase() in 'A'..'Z' -> true
            s.startsWith("_") -> true*/
            pattern.matcher(s).matches() -> true
            else -> false
        }
    }




    fun main() {
        println("What day is it today?")
        //println(dayOfWeek())
        //println(dayOfWeekSingleExp())

        println("Today is ${dayOfWeekSingleExp()}")



        println(isValidIdentifier("name"))   // true
        println(isValidIdentifier("_name"))  // true
        println(isValidIdentifier("_12"))    // true
        println(isValidIdentifier(""))       // false
        println(isValidIdentifier("012"))    // false
        println(isValidIdentifier("no$"))    // false
    }
}