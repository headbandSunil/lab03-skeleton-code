package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView

class ActivityOne : Activity() {

    private lateinit var onCreateCounterView: TextView
    private lateinit var onStartCounterView: TextView
    private lateinit var onResumeCounterView: TextView
    private lateinit var onPauseCounterView: TextView
    private lateinit var onStopCounterView: TextView
    private lateinit var onDestroyCounterView: TextView
    private lateinit var onRestartCounterView: TextView

    // lifecycle counts
    //TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called and log the info

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        onCreateCounterView = findViewById(R.id.onCreateCounts)
        onStartCounterView = findViewById(R.id.onStartCounts)
        onResumeCounterView = findViewById(R.id.onResumeCounts)
        onPauseCounterView = findViewById(R.id.onPauseCounts)
        onStopCounterView = findViewById(R.id.onStopCounts)
        onDestroyCounterView = findViewById(R.id.onDestroyCounts)
        onRestartCounterView = findViewById(R.id.onRestartCounts)


        // Restore stuff back
        val pref = getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = 0

        var cc = pref.getInt("onCreateCounter", defaultValue)
        var sc = pref.getInt("onStartCounter", defaultValue)
        var rc = pref.getInt("onResumeCounter", defaultValue)
        var pc = pref.getInt("onPauseCounter", defaultValue)
        var stc = pref.getInt("onStopCounter", defaultValue)
        var dc = pref.getInt("onDestroyCounter", defaultValue)
        var rsc = pref.getInt("onRestartCounter", defaultValue)

        onCreateCounterView.text = "$cc"
        onStartCounterView.text = "$sc"
        onResumeCounterView.text = "$rc"
        onPauseCounterView.text = "$pc"
        onStopCounterView.text = "$stc"
        onDestroyCounterView.text = "$dc"
        onRestartCounterView.text = "$rsc"


        updateTextViewValue(onCreateCounterView, "onCreate")
    }

    public override fun onStart() {
        super.onStart()

        updateTextViewValue(onStartCounterView, "onStart")
    }

    public override fun onResume() {
        super.onResume()
        updateTextViewValue(onResumeCounterView, "onResume")
    }

    public override fun onPause() {
        super.onPause()
        updateTextViewValue(onPauseCounterView, "onPause")


    }

    public override fun onStop() {
        super.onStop()
        updateTextViewValue(onStopCounterView, "onStop")
    }

    public override fun onDestroy() {
        super.onDestroy()
        updateTextViewValue(onDestroyCounterView, "onDestroy")


        // Save stuff
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {

            putInt("onCreateCounter", onCreateCounterView.text.toString().toInt())
            putInt("onStartCounter", onStartCounterView.text.toString().toInt())
            putInt("onResumeCounter", onResumeCounterView.text.toString().toInt())
            putInt("onPauseCounter", onPauseCounterView.text.toString().toInt())
            putInt("onStopCounter", onStopCounterView.text.toString().toInt())
            putInt("onDestroyCounter", onDestroyCounterView.text.toString().toInt())
            putInt("onRestartCounter", onRestartCounterView.text.toString().toInt())
            apply()
        }
    }

    public override fun onRestart() {
        super.onRestart()
        updateTextViewValue(onRestartCounterView, "onRestart")
    }

    private fun updateTextViewValue(onSomethingView: TextView, methodName: String) {
        val newValue = onSomethingView.text.toString().toInt() + 1
        Log.d(TAG, "$methodName counter: $newValue")
        onSomethingView.text = newValue.toString()
    }


    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    //Save instance state
    /*
    public override fun onSaveInstanceState(savedInstanceState: Bundle) {

        savedInstanceState?.run {
            /*putCharSequence("onCreateCounter", onCreateCounterView.text)
            putCharSequence("onStartCounter", onStartCounterView.text)
            putCharSequence("onPauseCounter", onPauseCounterView.text)
            putCharSequence("onResumeCounter", onResumeCounterView.text)
            putCharSequence("onStopCounter", onStopCounterView.text)
            putCharSequence("onDestroyCounter", onDestroyCounterView.text)
            putCharSequence("onRestartCounter", onRestartCounterView.text)*/

            putInt("onCreateCounter", onCreateCounter)
            putInt("onStartCounter", onStartCounter)
            putInt("onPauseCounter", onPauseCounter)
            putInt("onResumeCounter", onResumeCounter)
            putInt("onStopCounter", onStopCounter)
            putInt("onDestroyCounter", onDestroyCounter)
            putInt("onRestartCounter", onRestartCounter)
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        with(savedInstanceState) {
            onCreateCounter = savedInstanceState.getInt("onCreateCounter")
            onStartCounter = savedInstanceState.getInt("onStartCounter")
            onPauseCounter = savedInstanceState.getInt("onPauseCounter")
            onResumeCounter = savedInstanceState.getInt("onResumeCounter")
            onStopCounter = savedInstanceState.getInt("onStopCounter")
            onDestroyCounter = savedInstanceState.getInt("onDestroyCounter")
            onRestartCounter = savedInstanceState.getInt("onRestartCounter")

            onCreateCounterView.text = onCreateCounter.toString()
            onStartCounterView.text = onStartCounter.toString()
            onPauseCounterView.text = onPauseCounter.toString()
            onResumeCounterView.text = onResumeCounter.toString()
            onStopCounterView.text = onStopCounter.toString()
            onDestroyCounterView.text = onDestroyCounter.toString()
            onRestartCounterView.text = onRestartCounter.toString()
            /*onCreateCounterView.text = savedInstanceState.getCharSequence("onCreateCounter")
            onStartCounterView.text = savedInstanceState.getCharSequence("onStartCounter")
            onPauseCounterView.text = savedInstanceState.getCharSequence("onPauseCounter")
            onResumeCounterView.text = savedInstanceState.getCharSequence("onResumeCounter")
            onStopCounterView.text = savedInstanceState.getCharSequence("onStopCounter")
            onDestroyCounterView.text = savedInstanceState.getCharSequence("onDestroyCounter")
            onRestartCounterView.text = savedInstanceState.getCharSequence("onRestartCounter")*/
        }
    }*/


    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    fun resetCounters(view: View) {
        /*onCreateCounter = 0
        onStartCounter = 0
        onPauseCounter = 0
        onResumeCounter = 0
        onStopCounter = 0
        onDestroyCounter = 0
        onRestartCounter = 0*/

        onCreateCounterView.text = "0"
        onStartCounterView.text = "0"
        onPauseCounterView.text = "0"
        onResumeCounterView.text = "0"
        onStopCounterView.text = "0"
        onDestroyCounterView.text = "0"
        onRestartCounterView.text = "0"
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private const val TAG = "Lab-ActivityOne"

        // Counters
        private var onCreateCounter = 0
        private var onStartCounter = 0
        private var onPauseCounter = 0
        private var onResumeCounter = 0
        private var onStopCounter = 0
        private var onDestroyCounter = 0
        private var onRestartCounter = 0
    }


}
